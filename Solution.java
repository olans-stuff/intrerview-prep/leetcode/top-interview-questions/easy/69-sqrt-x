class Solution {
    public int mySqrt(int x) {
        // First, jus check 0 and 1, return each values as 0 or 1 to itself is itself
        if (x == 0 || x == 1) {
            return x;
        }

        int startPoint = 1;
        int end = x;
        int mid ;


        //bonary search lets say 64 is our number

        while ( startPoint <= end) { // while start < end which is our value
            mid = startPoint + (end -startPoint) / 2; // set mid to be the middle mid = 1 + (64 -1) / 2 = 32

            if ( (long) mid * mid > (long) x)  { // if 32 x 32> 64
                end = mid -1;   //decreease mid
            } else if ( (long )mid * mid == (long) x) { // 32 x 32 == 64
                return mid; //found it
            } else {
                startPoint = mid + 1; // we gone too far need uo start 
            }
        }
        return Math.round(end);
    }
}